package dictionaryOperations;
//find all words which contains a specific substring

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Set;
import Utils.Utils;

public class SearchDictionaryOperation implements DictionaryOperation{

    private  Set<String>wordsSet;
    private BufferedReader in;

    public SearchDictionaryOperation(Set<String> wordsSet, BufferedReader in){
        this.in= in;
        this.wordsSet= wordsSet;
    }
    @Override

    public void run() throws IOException {
        System.out.println("Searching......");
        String userInput=in.readLine();
        System.out.println("From user:v"+ userInput);

        Set<String>result= Utils.findWordsThatContainASpecificSubstring(wordsSet,userInput);

        for(String line:result){
            System.out.println(line);
        }

        System.out.println("Finished");

    }
}
