package Utils;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Utils {

    public static Set<String> removeDuplicates(List<String> allLines) {
        Set<String> wordsSet = new HashSet<>();

        for (String line : allLines) {
            wordsSet.add(line);
        }
        return wordsSet;
    }

    public static Set<String> findWordsThatContainASpecificSubstring(Set<String> wordSet, String word) {
        Set<String> result = new HashSet<>();
        for (String line : wordSet) {
            if (line.contains(word)) {
                result.add(line);
            }
        }
        return result;

    }

    public static Set<String> findAllPalindromes(Set<String> wordsSet) {
        Set<String> result = new HashSet<>();
        for (String line : wordsSet) {
            StringBuilder reverseWord = new StringBuilder(line).reverse();
            if (line.equals(reverseWord.toString())) {
                result.add(line);
            }
        }
        return result;
    }
}
