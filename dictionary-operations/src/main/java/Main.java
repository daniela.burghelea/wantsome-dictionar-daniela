import Utils.Utils;
import dictionaryOperations.DictionaryOperation;
import dictionaryOperations.PalindromeDictionaryOperation;
import dictionaryOperations.SearchDictionaryOperation;
import fileReader.ResourceInputFileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Main {

    public static void main(String[] args) throws IOException {

        System.out.println("This is my main class");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); //  input tastatura;
/*
         //citim de la tastatura
        String userInput = in.readLine();
        System.out.println("User Input: " + userInput);*/

        List<String> words = new ArrayList<>();
        ResourceInputFileReader resourceInputFileReader=new ResourceInputFileReader();
        words = resourceInputFileReader.readFile("dex.tx");
        Set<String> wordsNotDuplicate = Utils.removeDuplicates(words);

        while (true) {
            System.out.println("Menu: \n" +
                    "1.Search \n" +
                    "2.Find all palindroms \n " +
                    "3. Anagrame \n " +
                    "0. Exit");

            String userInput = in.readLine();
            if (userInput.equals("0")) {
                System.out.println("La revedere");
                break;
            }

            DictionaryOperation operation = null;

            switch (userInput) {
                case "1":
                    operation = new SearchDictionaryOperation(wordsNotDuplicate, in);
                    break;
                case "2":
                    operation = new PalindromeDictionaryOperation(wordsNotDuplicate);
                    break;
                default:
                    System.out.println("Invalid operation");

            }
            if (operation != null) {
                operation.run();
            }
        }
           /* List<String> words = new ArrayList<>();
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        words = resourceInputFileReader.readFile("dex.txt");*/
   /*     for (String word:words){
            System.out.println(word);
        }*/

     /*   Set<String> wordsNotDuplicate=  Utils.Utils.removeDuplicates(words);
        for(String word: wordsNotDuplicate) {
            System.out.println(word);
        }
        */
    }
}
